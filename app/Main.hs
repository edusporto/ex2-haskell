{-# LANGUAGE OverloadedStrings #-}

import Application ()
-- for YesodDispatch instance

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Logger (runStdoutLoggingT)
import Database.Persist.Postgresql
import Foundation
import Yesod.Core
import Yesod.Static

import System.Environment (getEnv)

connStr = "dbname=de61l38u7tre18 host=ec2-52-44-46-66.compute-1.amazonaws.com user=ukxlgtfoswekxc password=d3720dc3517e63092df93ad80db28a3e7300bc08c3b698ea9f3fcd01cffe5342 port=5432"

main :: IO ()
main = runStdoutLoggingT $
  withPostgresqlPool connStr 10 $ \pool -> liftIO $ do
    flip runSqlPersistMPool pool $ do
      runMigration migrateAll
    static@(Static settings) <- static "static"
    port <- getEnv "PORT"
    warp (read port) (App pool static)
