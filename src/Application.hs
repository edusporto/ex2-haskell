{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE TemplateHaskell      #-}
{-# LANGUAGE ViewPatterns         #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}
module Application where

import Foundation
import Yesod.Core

import Home

import Handler.Pessoa.Pessoa
import Handler.Pessoa.PessoaCadastro
import Handler.Pessoa.Pessoas
import Handler.Pessoa.PessoaRemove
import Handler.Pessoa.PessoaAtualiza

import Handler.Linguagem.Linguagem
import Handler.Linguagem.LinguagemCadastro
import Handler.Linguagem.Linguagens
import Handler.Linguagem.LinguagemRemove
import Handler.Linguagem.LinguagemAtualiza

import Handler.Programador.Programador
import Handler.Programador.ProgramadorCadastro
import Handler.Programador.Programadores ( getProgramadoresR )
import Handler.Programador.ProgramadorRemove
import Handler.Programador.ProgramadorAtualiza

mkYesodDispatch "App" resourcesApp
