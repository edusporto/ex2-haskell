{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Handler.Linguagem.Linguagem where

import Foundation
import Yesod
import Yesod.Core

getLinguagemR :: LinguagemId -> Handler Html
getLinguagemR linguagemId = do
  linguagem <- runDB $ get404 linguagemId
  defaultLayout
    [whamlet| 
        <p>
            #{show linguagem}
    |]

postLinguagemR :: LinguagemId -> Handler TypedContent
postLinguagemR linguagemId = do
    linguagem <- runDB $ get404 linguagemId
    selectRep $ do
        provideJson $ object ["result" .= linguagem]
