{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Linguagem.LinguagemAtualiza where

import Control.Applicative
import Data.Text (Text)
import Foundation
  ( EntityField (LinguagemCriador, LinguagemNome, LinguagemTipagem),
    Handler,
    Linguagem (Linguagem, linguagemCriador, linguagemNome, linguagemTipagem),
    LinguagemId,
    Route (LinguagemAtualizaR),
  )
import Yesod
import Yesod.Core

getLinguagemAtualizaR :: LinguagemId -> Handler Html
getLinguagemAtualizaR pid = do
  linguagem <- runDB $ get404 pid
  let nome = linguagemNome linguagem
      tipagem = linguagemTipagem linguagem
      criador = linguagemCriador linguagem

  defaultLayout
    [whamlet| 
        <form acion=@{LinguagemAtualizaR pid} method=post>
            Nome
            <input type=text name=nome value=#{nome}>
            <br>
            Tipagem (estática, dinâmica, forte, fraca...)
            <input type=text name=tipagem value=#{tipagem}>
            <br>
            Criador
            <input type=text name=criador value=#{criador}>
            <br>
            <input type=submit value=alterar>
    |]

postLinguagemAtualizaR :: LinguagemId -> Handler Html
postLinguagemAtualizaR pid =
  runInputPost
    ( Linguagem <$> ireq textField "nome" <*> ireq textField "tipagem"
        <*> ireq textField "criador"
    )
    >>= \linguagemForm ->
      runDB (get404 pid)
        >>= \linguagemDadosAntigos ->
          runDB
            ( update
                pid
                [ LinguagemNome =. linguagemNome linguagemForm,
                  LinguagemTipagem =. linguagemTipagem linguagemForm,
                  LinguagemCriador =. linguagemCriador linguagemForm
                ]
            )
            >> defaultLayout
              [whamlet|
            <p>
                Antes: #{show linguagemDadosAntigos}
            <p>
                Depois: #{show linguagemForm}
        |]
