{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
module Handler.Linguagem.LinguagemCadastro where

import Foundation
import Yesod.Core
import Yesod
import Control.Applicative
import Data.Text (Text)

getLinguagemCadastroR :: Handler Html
getLinguagemCadastroR = defaultLayout
    [whamlet|
        <form action=@{LinguagemCadastroR} method=post>
            <p>
                Nome
                <input type=text name=nome>
                <br>
                Tipagem (estática, dinâmica, forte, fraca...)
                <input type=text name=tipagem>
                <br>
                Criador
                <input type=text name=criador>
                <br>
                <input type=submit value="Cadastrar">
    |]

postLinguagemCadastroR :: Handler Html
postLinguagemCadastroR = do
    linguagem <- runInputPost $ Linguagem
                <$> ireq textField "nome"
                <*> ireq textField "tipagem"
                <*> ireq textField "criador"
                
    pid <- runDB $ insert linguagem
    defaultLayout 
        [whamlet|
            <p>
                #{show linguagem}
            <p>
                O id da linguagem cadastrada: #{show pid}
        |]