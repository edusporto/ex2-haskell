{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Linguagem.LinguagemRemove where

import Control.Applicative
import Data.Text (Text)
import Foundation
import Yesod
import Yesod.Core

-- Por query string ou por formulario
getLinguagemRemoveR :: LinguagemId -> Handler Html
getLinguagemRemoveR pid = do
  linguagem <- runDB $ get404 pid
  runDB $ delete pid
  defaultLayout
    [whamlet| 
        <p>
            A linguagem #{ show linguagem } foi removida com sucesso
    |]

postLinguagemRemoveR :: LinguagemId -> Handler Html
postLinguagemRemoveR pid = undefined
