{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Linguagem.Linguagens where

import Control.Applicative
import Data.Text (Text)
import Foundation
import Yesod
import Yesod.Core

getLinguagensR :: Handler Html
getLinguagensR = do
  linguagensEntity <- runDB $ selectList [] [Desc LinguagemNome]
  defaultLayout
    [whamlet|
        $forall (Entity pid linguagem) <- linguagensEntity
            <p> 
                #{show $ linguagemNome linguagem}
                <a href=@{LinguagemR pid }>
                    Dados detalhados
                <a href=@{LinguagemRemoveR pid} >
                    Remover Linguagem
                <a href=@{LinguagemAtualizaR pid}>
                    Atualizar dados da Linguagem
    |]
