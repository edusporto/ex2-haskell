{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Handler.Pessoa.Pessoa where

import Foundation
import Yesod
import Yesod.Core

getPessoaR :: PessoaId -> Handler Html
getPessoaR pessoaId = do
  pessoa <- runDB $ get404 pessoaId
  defaultLayout
    [whamlet| 
        <p>
            #{show pessoa}
    |]

postPessoaR :: PessoaId -> Handler TypedContent
postPessoaR pessoaId = do
  pessoa <- runDB $ get404 pessoaId
  selectRep $ do
    provideJson $ object ["result" .= pessoa]
