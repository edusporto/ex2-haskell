{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Pessoa.PessoaAtualiza where

import Control.Applicative
import Data.Text (Text)
import Foundation
import Yesod
import Yesod.Core

getPessoaAtualizaR :: PessoaId -> Handler Html
getPessoaAtualizaR pid = do
  pessoa <- runDB $ get404 pid
  let nome = pessoaNome pessoa
      sobrenome = pessoaSobreNome pessoa
      idade = pessoaIdade pessoa

  defaultLayout
    [whamlet| 
        <form acion=@{PessoaAtualizaR pid} method=post>
            Nome
            <input type=text name=nome value=#{nome}>
            <br>
            Sobre nome
            <input type=text name=sobrenome value=#{sobrenome}>
            <br>
            Idade
            <input type=number name=idade value=#{idade}>
            <br>
            <input type=submit value=alterar>
    |]

postPessoaAtualizaR :: PessoaId -> Handler Html
postPessoaAtualizaR pid =
  runInputPost
    ( Pessoa
        <$> ireq textField "nome"
        <*> ireq textField "sobrenome"
        <*> ireq intField "idade"
    )
    >>= \pessoaForm ->
      runDB (get404 pid)
        >>= \pessoaDadosAntigos ->
          runDB
            ( update
                pid
                [ PessoaNome =. pessoaNome pessoaForm,
                  PessoaSobreNome =. pessoaSobreNome pessoaForm,
                  PessoaIdade =. pessoaIdade pessoaForm
                ]
            )
            >> defaultLayout
              [whamlet|
            <p>
                Antes: #{show pessoaDadosAntigos}
            <p>
                Depois: #{show pessoaForm}
        |]
