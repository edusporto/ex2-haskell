{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Pessoa.Pessoas where

import Control.Applicative
import Data.Text (Text)
import Foundation
import Yesod
import Yesod.Core

getPessoasR :: Handler Html
getPessoasR = do
  pessoasEntity <- runDB $ selectList [] [Desc PessoaNome]
  defaultLayout
    [whamlet|
        $forall (Entity pid pessoa) <- pessoasEntity
            <p> 
                #{show $ pessoaNome pessoa}
                <a href=@{PessoaR pid }>
                    Dados detalhados
                <a href=@{PessoaRemoveR pid} >
                    Remover Pessoa
                <a href=@{PessoaAtualizaR pid}>
                    Atualizar dados da Pessoa
    |]
