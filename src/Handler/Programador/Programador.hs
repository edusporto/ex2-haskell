{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Handler.Programador.Programador where

import Foundation
import Yesod
import Yesod.Core

getProgramadorR :: ProgramadorId -> Handler Html
getProgramadorR programadorId = do
  programador <- runDB $ get404 programadorId
  pessoa <- runDB $ get404 (programadorPessoaId programador)
  linguagem <- runDB $ get404 (programadorLinguagemId programador)

  defaultLayout
    [whamlet|
        <p>
            Dados do Programador:
            #{show programador}

        <p>
            Dados da Pessoa:
            #{show pessoa}u

        <p>
            Dados da Linguagem:
            #{show linguagem}
    |]

postProgramadorR :: ProgramadorId -> Handler TypedContent
postProgramadorR programadorId = do
  programador <- runDB $ get404 programadorId
  selectRep $ do
    provideJson $ object ["result" .= programador]
