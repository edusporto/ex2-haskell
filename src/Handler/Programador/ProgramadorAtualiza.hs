{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Programador.ProgramadorAtualiza where

import Control.Applicative
import Data.Text (Text)
import Data.Text.Read
import Database.Persist.Sql
import Foundation
import Yesod
import Yesod.Core

-- Bibliografia: https://stackoverflow.com/questions/46037708/yesod-form-with-foreign-key

pessoaIdField :: (Monad m, RenderMessage (HandlerSite m) FormMessage) => Field m (Key Pessoa)
pessoaIdField =
  Field
    { fieldParse = parseHelper $ \s ->
        case signed decimal s of
          Right (a, "") -> Right $ toSqlKey a
          _ -> Left $ MsgInvalidInteger s,
      fieldView = \_ _ _ _ _ -> "",
      fieldEnctype = UrlEncoded
    }

linguagemIdField :: (Monad m, RenderMessage (HandlerSite m) FormMessage) => Field m (Key Linguagem)
linguagemIdField =
  Field
    { fieldParse = parseHelper $ \s ->
        case signed decimal s of
          Right (a, "") -> Right $ toSqlKey a
          _ -> Left $ MsgInvalidInteger s,
      fieldView = \_ _ _ _ _ -> "",
      fieldEnctype = UrlEncoded
    }



getProgramadorAtualizaR :: ProgramadorId -> Handler Html
getProgramadorAtualizaR pid = do
  programador <- runDB $ get404 pid
  let pessoaId = programadorPessoaId programador
      linguagemId = programadorLinguagemId programador

  defaultLayout
    [whamlet|
        <form acion=@{ProgramadorAtualizaR pid} method=post>
            PessoaId
            <input type=number name=pessoaId value=#{show pessoaId}>
            <br>
            LinguagemId
            <input type=number name=linguagemId value=#{show linguagemId}>
            <br>
            <input type=submit value=alterar>
    |]

postProgramadorAtualizaR :: ProgramadorId -> Handler Html
postProgramadorAtualizaR pid =
  runInputPost
    ( Programador
        <$> ireq pessoaIdField "pessoaId"
        <*> ireq linguagemIdField "linguagemId"
    )
    >>= \programadorForm ->
      runDB (get404 pid)
        >>= \programadorDadosAntigos ->
          runDB
            ( update
                pid
                [ ProgramadorPessoaId =. programadorPessoaId programadorForm,
                  ProgramadorLinguagemId =. programadorLinguagemId programadorForm
                ]
            )
            >> defaultLayout
              [whamlet|
                  <p>
                      Antes: #{show programadorDadosAntigos}
                  <p>
                      Depois: #{show programadorForm}
              |]
