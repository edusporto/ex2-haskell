{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Programador.ProgramadorCadastro where

import Control.Applicative
import Data.Text (Text)
import Data.Text.Read
import Database.Persist.Sql
import Foundation
import Yesod
import Yesod.Core

pessoaIdField :: (Monad m, RenderMessage (HandlerSite m) FormMessage) => Field m (Key Pessoa)
pessoaIdField =
  Field
    { fieldParse = parseHelper $ \s ->
        case signed decimal s of
          Right (a, "") -> Right $ toSqlKey a
          _ -> Left $ MsgInvalidInteger s,
      fieldView = \_ _ _ _ _ -> "",
      fieldEnctype = UrlEncoded
    }

linguagemIdField :: (Monad m, RenderMessage (HandlerSite m) FormMessage) => Field m (Key Linguagem)
linguagemIdField =
  Field
    { fieldParse = parseHelper $ \s ->
        case signed decimal s of
          Right (a, "") -> Right $ toSqlKey a
          _ -> Left $ MsgInvalidInteger s,
      fieldView = \_ _ _ _ _ -> "",
      fieldEnctype = UrlEncoded
    }

getProgramadorCadastroR :: Handler Html
getProgramadorCadastroR =
  defaultLayout
    [whamlet|
        <form action=@{ProgramadorCadastroR} method=post>
            <p>
                PessoaId
                <input type=number name=pessoaId>
                <br>
                LinguagemId
                <input type=number name=linguagemId>
                <br>
                <input type=submit value="Cadastrar">
    |]

postProgramadorCadastroR :: Handler Html
postProgramadorCadastroR = do
  programador <-
    runInputPost $
      Programador
        <$> ireq pessoaIdField "pessoaId"
        <*> ireq linguagemIdField "linguagemId"

  pid <- runDB $ insert programador
  defaultLayout
    [whamlet|
            <p>
                #{show programador}
            <p>
                O id da programador cadastrada: #{show pid}
        |]