{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Programador.ProgramadorRemove where

import Control.Applicative
import Data.Text (Text)
import Foundation
import Yesod
import Yesod.Core

-- Por query string ou por formulario
getProgramadorRemoveR :: ProgramadorId -> Handler Html
getProgramadorRemoveR pid = do
  programador <- runDB $ get404 pid
  runDB $ delete pid
  defaultLayout
    [whamlet| 
        <p>
            O programador #{ show programador } foi removido com sucesso
    |]
