{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Programador.Programadores where

import Control.Applicative
import Data.Text (Text)
import Foundation
import Yesod
import Yesod.Core

getProgramadoresR :: Handler Html
getProgramadoresR = do
  programadoresEntity <- runDB $ selectList [] [Desc ProgramadorId]
  defaultLayout
    [whamlet|
        $forall (Entity pid programador) <- programadoresEntity
            <p>
                #{show $ programadorPessoaId programador}
                <a href=@{ProgramadorR pid }>
                    Dados detalhados
                <a href=@{ProgramadorRemoveR pid} >
                    Remover Programador
                <a href=@{ProgramadorAtualizaR pid}>
                    Atualizar dados da Programador
    |]
