{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Home where

import Foundation
import Stylesheet (myCSS)
import Yesod
import Yesod.Core

getHomeR :: Handler Html
getHomeR = do
  pessoas <- runDB $ selectList [] [Asc PessoaIdade]

  defaultLayout $ do
    setTitle "Projeto Haskell-Yesod"

    -- Carrega CSS
    toWidget myCSS

    -- Imagem `Haskell`
    [whamlet|
        <img src=@{StaticR haskell_jpg}/>
    |]

    -- Introdução
    [whamlet|
        <marquee><h1>Bem-vindo!</h1></marquee>

        <b>Aluno: Eduardo Sandalo Porto</b>

        <br>
    |]

    -- Tabela Pessoa
    [whamlet|
      <h2>Tabela Pessoa</h2>

      <p>
        <a href=@{PessoasR}>Pessoas</a>
      <p>
        <a href=@{PessoaCadastroR}>PessoaCadastro</a>
      
      <h3>Rotas interessantes: </h3>
        <pre>/pessoa/#PessoaId            (GET/POST)</pre>          
        <pre>/pessoa/atualiza/#PessoaId   (GET/POST)</pre> 
        <pre>/pessoa/remove/#PessoaId     (GET)</pre>
      
      <br>
    |]

    -- Tabela Linguagem
    [whamlet|
      <h2>Tabela Linguagem</h2>

      <p>
        <a href=@{LinguagensR}>Linguagens
      <p>
        <a href=@{LinguagemCadastroR}>LinguagemCadastro
      
      <h3>Rotas interessantes: </h3>
        <pre>/linguagem/#LinguagemId          (GET/POST)</pre>        
        <pre>/linguagem/atualiza/#LinguagemId (GET/POST)</pre>
        <pre>/linguagem/remove/#LinguagemId   (GET/POST)</pre>
      
      <br>
    |]

    -- Tabela Programador
    --   -> Usa as tabelas Pessoa e Linguagem
    [whamlet|
      <h2>Tabela Programador</h2>

      <p>
        <a href=@{ProgramadoresR}>Programadores
      <p>
        <a href=@{ProgramadorCadastroR}>ProgramadorCadastro
      
      <h3>Rotas interessantes: </h3>
        <pre>/programador/#ProgramadorId          (GET/POST)</pre>
        A rota anterior é especial, pois executa uma query relacional no banco de dados para apresentar a Pessoa e a Linguagem atrelados a um Programador
        <pre>/programador/atualiza/#ProgramadorId (GET/POST)</pre>
        <pre>/programador/remove/#ProgramadorId   (GET/POST)</pre>
      
      <br>
    |]

    -- Informações extra
    [whamlet|
      <h2>Informações extras</h2>

      <h3>Feito: </h3>
      <ul>
        <li>Três tabelas com relacionamento n para m</li>
        <li>Operações CRUD com query 1 para n</li>
        <li>Uso de static files, interpoladores de rota e de tipos, formulários, query string</li>
        <li>Uso da arquitetura REST para três rotas: /pessoa, /linguagem, /programador
    
      <h3>Não feito:</h3>
      <ul>
        <li>Uso de sessão, autenticação e autorização</li>
    |]
